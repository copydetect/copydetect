/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

public class RecursiveUnTar {
	String startpt ;
	String cmd = "ls -R";
	String assignmentName;
        
	/*public RecursiveUnTar(String sp){
		startpt = sp;
		if(!startpt.endsWith("/")){
			startpt += "/";
		}
	}*/
        
        public RecursiveUnTar(String sp,String assignmentName){
		startpt = sp;
		if(!startpt.endsWith("/")){
			startpt += "/";
		}
                this.assignmentName=assignmentName;
	}
        
	public int UnTar() throws Exception{
		System.out.println("Untar started");
                File workDir = new File(startpt);
		Process p = Runtime.getRuntime().exec(cmd, null, workDir);
		int i = p.waitFor();
                System.out.println("Untar started1");
                int num=0;
		if(i == 0){
			BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String ip;
			String currdir = startpt;
			while((ip = br.readLine()) != null){
				if((ip.startsWith("./")) && (ip.endsWith(":"))){
					currdir = startpt + ip.substring(2, ip.length()-1) + "/";
				}else{
                                        String[] userFileDetails= ip.split("_");
                                        String outputDir;
                                        if(userFileDetails.length>=2){
                                                outputDir=currdir+userFileDetails[0]+"_"+userFileDetails[1]+"/";
                                        } else {
                                            if(currdir.endsWith("/"))
                                                outputDir=currdir;
                                            else 
                                                outputDir=currdir+"/";
                                        }
                                        outputDir=currdir;
                                        outputDir=outputDir.replaceAll(" ", "_");
                                        if(assignmentName!=null && !assignmentName.equals("")) {
                                            outputDir+=assignmentName+"/";
                                        }
					if(ip.endsWith(".tar") || ip.endsWith(".tar.gz") || ip.endsWith(".tgz") ){

                                                num++;
                                                Process mk = Runtime.getRuntime().exec("mkdir " + outputDir);
                                                mk.waitFor(); 
                                                mk.destroy();
                                                String mvcmd[]={"mv",currdir+ip,outputDir};
                                                Process mv = Runtime.getRuntime().exec(mvcmd);
                                                mv.waitFor();                                            
                                                mv.destroy();
                                                
						String tarF = outputDir + ip;                                            
						String tarcmd[]={"tar","-xf",tarF,"-C",outputDir};
                                                Process tt = Runtime.getRuntime().exec(tarcmd);
						tt.waitFor();
                                                tt.destroy();
						
                                                String rmcmd[]={"rm",tarF};
                                                Process rr = Runtime.getRuntime().exec(rmcmd);
						rr.waitFor();
                                                rr.destroy();
					}else if(ip.endsWith(".zip")) {
                                            
                                                num++;
                                                //Process mk = Runtime.getRuntime().exec("mkdir " + outputDir);
                                                Process mk=Runtime.getRuntime().exec(new String[]{"mkdir",outputDir});
                                                mk.waitFor();
                                                String mvcmd[]={"mv",currdir+ip,outputDir};
                                                Process mv = Runtime.getRuntime().exec(mvcmd);
                                                mv.waitFor();
                                                mv.destroy();
                                                
						String tarF = outputDir + ip;
                                                String[] zipcmd={"unzip",tarF,"-d", outputDir};
                                                Process zz = Runtime.getRuntime().exec(zipcmd);
						zz.waitFor();
                                                zz.destroy();
                                                
                                                String rmcmd[]={"rm",tarF};
                                                Process rr = Runtime.getRuntime().exec(rmcmd);
						rr.waitFor();
                                                rr.destroy();
					} else if(ip.endsWith(".java")){
                                                Process mk=Runtime.getRuntime().exec(new String[]{"mkdir",outputDir});
                                                mk.waitFor(); 
                                                mk.destroy();
                                                
                                                 String mvcmd[]={"mv",currdir+ip,outputDir};
                                                Process mv = Runtime.getRuntime().exec(mvcmd);
                                                mv.waitFor();
                                                mv.destroy();
                                        }
				}
			}
		}
                p.destroy();
                return num;
	}
        
	public static void main(String[] args){

		try{
			String startpt = "";
			//start point can be given as cmd line arg
			if(args.length > 0){
				if(args[0].equals("-sp")){
					startpt = args[1];
				}
			}
			RecursiveUnTar rut = new RecursiveUnTar(startpt,null);
			rut.UnTar();
		}catch(Exception e){
			System.out.println("Exception" + e);
                        e.printStackTrace();
			System.out.println("usage: java RecursiveUnTar -sp <startPointOfDir>");
		}
	}
}
