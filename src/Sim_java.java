
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Sim_java {

    String startPt;
    String output;

    public Sim_java(String wd, String op) {
        startPt = wd;
        output = op;
        if (!startPt.endsWith("/")) {
            startPt += "/";
        }
    }

    public int ExecSim(String prefix) throws Exception {
        ArrayList<String> argList = new ArrayList<String>();
        File workDir_ls = new File(startPt+prefix);
        Process p = Runtime.getRuntime().exec("ls -R", null, workDir_ls);

        BufferedReader lsbr = new BufferedReader(new InputStreamReader(p.getInputStream()));

        // BufferedReader lsbr = new BufferedReader(new FileReader(new File(startPt+prefix+"lsOut")));
        BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String ip;
        String currdir = "";
        while ((ip = br.readLine()) != null) {
            if ((ip.startsWith("./")) && (ip.endsWith(":"))) {
                currdir = ip.substring(2, ip.length() - 1) + "/";
            } else {
                String fullName = startPt + prefix + "/" + currdir + ip;
                if (FileType.isJava(fullName)) {
                    String javaF = currdir + ip;
                    argList.add(javaF);
                }
            }
        }

        int w = p.waitFor();
        System.out.println("Listed all files to process........");
        if (w == 0) {
            /*BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String ip;
			String currdir = "";
			while((ip = br.readLine()) != null){
                            if((ip.startsWith("./")) && (ip.endsWith(":"))){
					currdir = ip.substring(2, ip.length()-1) + "/";
				}else{
					String fullName = startPt + prefix + "/" + currdir + ip;
					if(FileType.isJava(fullName)){
						String javaF = currdir + ip;
						argList.add(javaF);
					}
				}
			}*/
            System.out.println("Number of java files = " + argList.size());
            //the list of java files are now present in the argList
            /*String[] sim_cmd = new String[argList.size() + 6];
			sim_cmd[0] = Config.sim_java;
			sim_cmd[1] = "-s";
			sim_cmd[2] = "-w";
			sim_cmd[3] = "200";
			sim_cmd[4] = "-o";
			sim_cmd[5] = output;*/
            String[] base_sim_cmd = Config.getSimBaseCommandArgs(Language.fromString("java"), startPt, prefix);

            String[] sim_cmd = new String[base_sim_cmd.length + argList.size()];
            System.arraycopy(base_sim_cmd, 0, sim_cmd, 0, base_sim_cmd.length);

            for (int i = 0; i < argList.size(); i++) {
                sim_cmd[i + base_sim_cmd.length] = prefix + "/" + argList.get(i);
            }
            File workDir = new File(startPt);
            Process tt = Runtime.getRuntime().exec(sim_cmd, null, workDir);
            tt.waitFor();
            System.out.println("Output of running sim--" + tt.exitValue());
        }
        return argList.size();
    }
}
