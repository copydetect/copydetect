

import java.util.ArrayList;

public class SumFileData {
	int runLen; //total runlength of the detected copy
	ArrayList<SnippetData> snippetList; //list of snippets (the two file names and line number of snippet) of the detected copies in the original sim_output file
	public SumFileData(){
		runLen = 0;
		snippetList = new ArrayList<SnippetData>();
	}
	public SumFileData(int rl, SnippetData sd){
		runLen = rl;
		snippetList = new ArrayList<SnippetData>();
		snippetList.add(sd);
	}
	public void add(int rl, SnippetData sd){
		if(snippetList == null){
			runLen = 0;
			snippetList = new ArrayList<SnippetData>();
		}
		runLen += rl;
		snippetList.add(sd);		
	}
	public int GetTotalRunLength(){
		return runLen;
	}
	public ArrayList<SnippetData> GetLineNoList(){
		return snippetList;
	}
}
