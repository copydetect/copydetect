

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * <ul>
 * <li>Input is the moodle.xml file that is present in the moodle backup tar file.
 * <li>Parses the xml file to obtain the moodle-id to assignment name mapping, and 
 * <li>the moodle-id to student name, roll number mapping.
 * </ul> 
 * @author rosecatherinek
 */
public class MoodleXML {

	String moodleFileName;
	String studIDFileName;
	String assignIDFileName;
	
	public MoodleXML(String mf, String sf, String af){
		moodleFileName = mf;
		studIDFileName = sf;
		assignIDFileName = af;
	}
	public void Parse() throws Exception{
		Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(moodleFileName));
		doc.getDocumentElement().normalize();
		FindStudentIDMapping(doc);
		FindAssignIDMapping(doc);
	}
	private void FindStudentIDMapping(Document doc) throws IOException{
		BufferedWriter bw = new BufferedWriter(new FileWriter(studIDFileName));
		NodeList listOfStudents = doc.getElementsByTagName("USER");
        int totalStudents = listOfStudents.getLength();
        System.out.println("Total no of students : " + totalStudents);

        for(int s = 0; s < listOfStudents.getLength(); s++){
            Node studNode = listOfStudents.item(s);
            if(studNode.getNodeType() == Node.ELEMENT_NODE){
            	try{
            		Element studElement = (Element)studNode;
            		Element moodleIDElt = (Element) studElement.getElementsByTagName("ID").item(0);
            		Element rollNoElt = (Element) studElement.getElementsByTagName("IDNUMBER").item(0);
            		Element firstNameElt = (Element) studElement.getElementsByTagName("FIRSTNAME").item(0);
            		Element lastNameElt = (Element) studElement.getElementsByTagName("LASTNAME").item(0);

            		String moodleID = moodleIDElt.getChildNodes().item(0).getNodeValue().trim();
            		String rollNo = rollNoElt.getChildNodes().item(0).getNodeValue().trim();
            		String firstName = firstNameElt.getChildNodes().item(0).getNodeValue().trim();
            		String lastName = lastNameElt.getChildNodes().item(0).getNodeValue().trim();
            		String name = firstName + " " + lastName;
            		bw.write(moodleID + " | " + name + " | " + rollNo);
            		bw.newLine();
            	}catch(Exception e){
            		//skip this record
            	}
            }
        }
        bw.close();
	}
	private void FindAssignIDMapping(Document doc) throws IOException{
		BufferedWriter bw = new BufferedWriter(new FileWriter(assignIDFileName));
		Element moodleDetails = (Element) doc.getElementsByTagName("DETAILS").item(0);
		NodeList listOfAssignments = moodleDetails.getElementsByTagName("INSTANCE");
		int totalAssignments = listOfAssignments.getLength();
		System.out.println("Total no of assignments : " + totalAssignments);
		for(int i = 0; i < listOfAssignments.getLength(); i++){
			try{
				Element assignNode = (Element) listOfAssignments.item(i);
				Element assignIDElt = (Element) assignNode.getElementsByTagName("ID").item(0);
				Element assignNameElt = (Element) assignNode.getElementsByTagName("NAME").item(0);

				String assignID = assignIDElt.getChildNodes().item(0).getNodeValue().trim();
				String assignName = assignNameElt.getChildNodes().item(0).getNodeValue().trim();
				bw.write(assignID + " | " + assignName);
				bw.newLine();
			}catch(Exception e){
				//skip this record
			}
		}
		bw.close();
	}
	public static void main(String[] args) {
		try{
			MoodleXML mxml = new MoodleXML("/home/naseer/ascwork/copydetect/moodle.xml", "studIDMapping.txt", "assignIDMapping.txt");
			mxml.Parse();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
