

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class ResponseWriter {
	PrintWriter out = null;
	
	public ResponseWriter(PrintWriter w){
		out = w;
	}
	public void WriteHeader(){
		out.println("<html>");
		out.println("<head><title> Copy Detection </title> </head>");
		out.println("<body>");
		out.println("<center>");
		out.println("<br><br>");
		out.println("<h3>Copy Detection</h3><br>");
		//out.println("<p>&nbsp;</p> <p>&nbsp;</p>");
		out.flush();
	}
	public void WriteFooter(){
		out.println("<p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p>"); 
		out.println("<small>for any queries/comments/suggestions, please mail to bikash@cse.iitb.ac.in,  <br>");
		out.println("</center>");
		out.println("</body>");
		out.println("</html>");
		out.flush();
		out.close();
	}
	public void WriteError(String errStr){
		String s = "<br><h4><font color=red>" + errStr + "</font></h4><br>";
		out.write(s);
		out.flush();
	}
	public void WriteStatus(String statStr){
		String s = "<br>" + statStr;
		out.write(s);
		out.flush();
	}
	public void WriteUploadForm(){
		out.println("<center>");
		out.println("<form action=\"Upload\" enctype=\"multipart/form-data\" method=\"post\">");
		out.println("<input name=\"datafile\" type=\"file\"> <br>");
		out.println("<input type=\"submit\" value=\"upload\">");
		out.println("</form>");
		out.println("</center>");
	}
	public void WriteProceedForm(String workDirName,String assignmentName){
		out.println("<p>&nbsp;</p>");
		out.println("<form action=\"SimProcess\" method=\"get\">");
		out.println("<input type=\"hidden\" name=\"dir\" value=\"" + workDirName + "\">");
		out.println("<input type=\"hidden\" name=\"state\" value=\"start\">");
                out.println("<input type=\"hidden\" name=\"assignmentName\" value=\""+assignmentName+"\">");
		out.println("<input type=\"submit\" value=\"proceed >>\">");
		out.println("</form>");
	}
	
        
        public void WriteTemplate(String assignName, String workDirName) throws Exception{
		out.println("<form action=\"SimProcess\" method=\"post\" target=\"_blank\">");
		
		out.println("<b>Assignment: " +assignName + "</b>");
		
		out.println("<br><br>Select the language of assignment submission:");
		out.println("<input type=\"radio\" name=\"lang\" value=\"java\" > Java " );
		out.println("<input type=\"radio\" name=\"lang\" value=\"c\" > C, C++ " );
                out.println("<input type=\"radio\" name=\"lang\" value=\"lisp\" > Lisp/Racket " );
		out.println("<input type=\"radio\" name=\"lang\" value=\"text\" checked> Text " );
		out.println("<br><br>");
		
		out.println("<input type=\"hidden\" name=\"dir\" value=\"" + workDirName + "\">");
                out.println("<input type=\"hidden\" name=\"assignmentName\" value=\"" + assignName + "\">");
                out.println("<p>Place the template code here </p>");
                out.println("<textarea rows=\"30\" cols=\"100\" name=\"templateCode\" ></textarea><br/><br/>");
		out.println("<input type=\"hidden\" name=\"state\" value=\"runsim\">");
                out.println("<input type=\"submit\" value=\"test similarity >>\">");
		out.println("</form>");
		
	}
        
        
	public String WriteAssignmentName(String assignIDMap, String assignID)throws Exception{
		String assignName = "";
		assignID = assignID.trim();
		BufferedReader br = new BufferedReader(new FileReader(assignIDMap));
		String line = "";
		while((line = br.readLine()) != null){
			StringTokenizer st = new StringTokenizer(line, "|");
			String id = st.nextToken().trim();
			if(id.equals(assignID)){
				assignName = st.nextToken().trim();
				out.println("<br><b>" + assignName + "</b><br><br>");
				break;
			}
		}
		out.flush();
		br.close();
		return assignName;
	}
	public void WriteUploadInstructionsTableFormat(){
		out.println("<table><tr><td>");
		out.println("<font color=#357EC7><b>Instructions:</b></font><br>");
		out.println("First back up the assignment(s) you wish to run copy detection on (see instructions below), and save the <br>" +
				"generated zip file.  Then upload the zip file below. (Multiple assignments can be uploaded in a single zip file.)");
		out.println("<p>&nbsp;</p> <p>&nbsp;</p>");
		out.flush();
	}
	/*public void WriteBackupInstructionsTableFormat(){
		out.println("<p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p>");
		out.println("<font color=#357EC7><b>Instructions for taking backup of course files from moodle</b></font>");
		out.println("<ol>");
		out.println("<li>In the Administration frame on the left, click on backup.");
		out.println("<li>On the backup page:<ol>"); 
		out.println("<li>Click on Include None in two places at the top, to deselect all assignments and user data.");  
		out.println("<li>Select the assignment and user data box for just the assignment(s) you wish to do copy detection on.");
		out.println("<li>Scroll to the bottom of the page and set Users: Course, Logs: No, User Files: No and Course Files: No.");
		out.println("<li>Click on Continue.");
		out.println("</ol> <li>You should now see a page with backup details with the right number of submissions. Click on continue. <br>The backup then gets saved.");
		out.println("<li>Now click on continue, and  you will see all your backups.  <br>Click on the backup you just made to save it to your PC.  This is a zip file.");
		out.println("(It contains a file named moodle.xml <br>and a folder named moddata which are required for detecting copies.)");
		out.println("<li>Upload this backup file here.");
		out.println("</ol>");
		out.println("</tr></table>");
	}*/

        public void WriteBackupInstructionsTableFormat(){
		out.println("<p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p>");
		out.println("<font color=#357EC7><b>Instructions for taking backup of course files from moodle</b></font>");
		out.println("<ol>");
		out.println("<li>Go to a moodle assignment, and cllck on view responses.  Then, on the top left there is a dropdown which allows to download all assignments; a zip file is generated.");
		out.println("<li>Upload the zip file on the copy detection web page: <a href=\"http://info.cse.iitb.ac.in:8181/copydetect/\">http://info.cse.iitb.ac.in:8181/copydetect/</a><ol>"); 
		out.println("<li>Proceed to the next page to select the language and provide any template code if required. </li");
                out.println("<li>Click on test similarity to get the scores. </li");
                out.println("</ol>");
		out.println("</tr></table>");
	}


	public void WriteSimInstructions(){
		out.println("<table><tr><td>");
		out.println("<font color=#357EC7><b>Please note the following:</b></font><br>");
		out.println("<ol><li>Choosing Java or C, C++ will run the similarity tester on files of the corresponding language.");
		out.println("<li>Choosing Text will run the similarity tester on all files of text type (which includes java, c, cpp, xml, sql).");
		out.println("<li>Text similarity tester is not capable of detecting variable renaming, where as Java and C,C++ testers are.");
		out.println("<li>Type of the file (Java, C, Text) is determined using the 'file' command. Hence the similarity tester is run on" + 
				"<br>corresponding files, regardless of the file extension (w.r.t the first two points above).");
		out.println("<li>All cases reported here, will have to be manually checked to verify if it indeed is a copy case.");
		out.println("</ol></tr></table>");
		//out.println("<p>&nbsp;</p> ");
	}
}

