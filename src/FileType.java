import java.io.BufferedReader;
import java.io.InputStreamReader;


public class FileType {
	static String cString = "c program text"; //same for cpp
	static String javaString = "java program text";
	static String textString = "text";
	
	public static boolean isC(String filename) throws Exception{
		//assumption: filename = fully qualified path
		
		//if the file extension is proper, then decide accordingly
		String lowerCaseFN = filename.toLowerCase();
		if(lowerCaseFN.endsWith(".pdf")){
			return false;
		}
                if(lowerCaseFN.endsWith(".c") || lowerCaseFN.endsWith(".cc") || lowerCaseFN.endsWith(".h") || lowerCaseFN.endsWith(".cpp")){
			return true;
		}
		//otherwise use the file command
		ProcessBuilder pb = new ProcessBuilder("file","-b",  filename);
                Process pp= pb.start();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(pp.getInputStream()));
		String line = br.readLine();
		if(line.toLowerCase().contains(cString) ){
			return true;
		}
                pp.waitFor();
		return false;
	}
	public static boolean isJava(String filename) throws Exception{
		//assumption: filename = fully qualified path
		
		//if the file extension is proper, then decide accordingly
		String lowerCaseFN = filename.toLowerCase();
		if(lowerCaseFN.endsWith(".pdf")){
			return false;
		}
                if(lowerCaseFN.endsWith(".java")){
			return true;
		}
		//otherwise use the file command
		ProcessBuilder pb = new ProcessBuilder("file","-b",  filename);
                Process pp= pb.start();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(pp.getInputStream()));
		String line = br.readLine();
		if(line.toLowerCase().contains(javaString) ){
			return true;
		}
                pp.waitFor();
                br.close();
		return false;
	}
	
	public static boolean isText(String filename) throws Exception{
		//assumption: filename = fully qualified path
		
		//if the file extension is proper, then decide accordingly
		String lowerCaseFN = filename.toLowerCase();
		if(lowerCaseFN.endsWith(".txt") || lowerCaseFN.endsWith(".sql") || lowerCaseFN.endsWith(".xml") || lowerCaseFN.endsWith(".pdf")){
			return true;
		}
		//otherwise use the file command
		ProcessBuilder pb = new ProcessBuilder("file","-b",  filename);
                Process pp= pb.start();
               // Process pp = Runtime.getRuntime().exec("file -b " + filename);
            
		BufferedReader br = new BufferedReader(new InputStreamReader(pp.getInputStream()));
		String line = br.readLine();
                
		if(line.toLowerCase().indexOf(textString) >= 0 ){
			return true;
		}
                br.close();
                pp.waitFor();
		return false;
	}
        
        
        public static boolean isLisp(String filename) throws Exception{
		//assumption: filename = fully qualified path
		
		//if the file extension is proper, then decide accordingly
		String lowerCaseFN = filename.toLowerCase();
		if(lowerCaseFN.endsWith(".rkt")){
			return true;
		}
		
		return false;
	}

}
