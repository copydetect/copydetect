

/**
 * int_String version of QuickSort2Arrays where the index array is hashmap keys (String)
 * takes length of the array as an argument
 * @author rosecatherinek
 * @ref utils.QuickSort2Arrays
 */
public class MyQuickSortIntStringArrays {
	private static long comparisons = 0;
	private static long exchanges   = 0;
	private static int length;
	
	/**
	 * sorts index in the decreasing order of its corresponding value given in a ; 
	 * length gives the number of entries from the beginning of a and index, that need to be taken as valid; 
	 * @param a
	 * @param index
	 * @param length
	 */
	public static void quicksortIntString(int[] a, String[] keys, int len) {
		length = len;
		shuffleIntString(a, keys);                        // to guard against worst-case
		quicksortIntString(a, 0, length - 1, keys);
	}

	// quicksort a[left] to a[right]
	private static void quicksortIntString(int[] a, int left, int right, String[] keys) {
		if (right <= left) return;
		int i = partitionIntString(a, left, right, keys);
		quicksortIntString(a, left, i-1, keys);
		quicksortIntString(a, i+1, right, keys);
	}

	// partition a[left] to a[right], assumes left < right
	private static int partitionIntString(int[] a, int left, int right, String[] keys) {
		int i = left - 1;
		int j = right;
		while (true) {
			while (greaterIntString(a[++i], a[right]))      // find item on left to swap
				;                               // a[right] acts as sentinel
			while (greaterIntString(a[right], a[--j]))      // find item on right to swap
				if (j == left) break;           // don't go out-of-bounds
			if (i >= j) break;                  // check if pointers cross
			exchIntString(a, i, j, keys);                      // swap two elements into place
		}
		exchIntString(a, i, right, keys);                      // swap with partition element
		return i;
	}
	/**
	 * is x > y ?
	 * @param x
	 * @param y
	 * @return
	 */
	private static boolean greaterIntString(int x, int y) {
		comparisons++;
		return (x > y);
	}

	// exchange a[i] and a[j]
	private static void exchIntString(int[] a, int i, int j, String[] keys) {
		exchanges++;
		int swap = a[i];
		a[i] = a[j];
		a[j] = swap;
		
		String swapd = keys[i];
		keys[i] = keys[j];
		keys[j] = swapd;
	}

	// shuffle the array a[]
	private static void shuffleIntString(int[] a, String[] keys) {
		int N = length;
		for (int i = 0; i < N; i++) {
			int r = i + (int) (Math.random() * (N-i));   // between i and N-1
			exchIntString(a, i, r, keys);
		}
	}
	
	
	public static void main(String[] args) {

		long start = System.currentTimeMillis();
		long stop = System.currentTimeMillis();
		double elapsed = (stop - start) / 1000.0;

		int[] b = {3, 7 , 1, 9, 23, 67, 0, 99};
		String[] keys = new String[b.length];
		for (int i = 0 ; i < b.length ; i++) keys[i] = i + "_key";
		quicksortIntString(b, keys, b.length);
		
		stop = System.currentTimeMillis();
		elapsed = (stop - start) / 1000.0;
		System.out.println("Quicksort:   " + elapsed + " seconds");

		// print statistics
		System.out.println("Comparisons: " + comparisons);
		System.out.println("Exchanges:   " + exchanges);
		for (int i = 0 ; i < b.length ; i++){
			System.out.println(b[i] + " " + keys[i]);
		}
	}
}
