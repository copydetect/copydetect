import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class DisplayFile extends javax.servlet.http.HttpServlet {
	
	private static final long serialVersionUID = 1L;
	PrintWriter pw=null;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException {
		response.setContentType("text/plain");
		pw = response.getWriter();
		String workDir = request.getParameter("dir");
		String filename = request.getParameter("name");
                filename=filename.replace("%20", " ");
		String title = request.getParameter("title");
		Display(Config.tempDir + "/" + workDir + "/" + filename , filename);
	}
	
	public void Display(String file, String fileName){
		//title = title.replace('|', ' ');
		pw.println("--------------------------------------------------------------------------------------------------------------------");
		pw.println("\n" + fileName + "\n");
		pw.println("--------------------------------------------------------------------------------------------------------------------\n\n");
		try{
			BufferedReader br = new BufferedReader(new FileReader(file));
			String inLine = new String("");
			while((inLine = br.readLine()) != null){
				pw.println(inLine);
			}
			pw.flush();
			pw.close();
			br.close();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Exception "+e);
		}
	}
} 