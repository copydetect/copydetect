
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class DisplaySnippet extends javax.servlet.http.HttpServlet {

    private static final long serialVersionUID = 1L;

    PrintWriter pw = null;

    public void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response)
            throws javax.servlet.ServletException, java.io.IOException {
        response.setContentType("text/plain");
        pw = response.getWriter();
        //int count = 0;
        //count=Integer.parseInt(request.getParameter("count"));
        String workDir = request.getParameter("dir");
        String lang = request.getParameter("lang");
        String assignID = request.getParameter("assignid");
        String title = request.getParameter("title");
        String simFile = "";
        /*if (lang.equalsIgnoreCase("c")) {
            simFile = Config.tempDir + "/" + workDir + "/" + Config.sim_c_output + "_" + assignID;
        } else if (lang.equalsIgnoreCase("java")) {
            simFile = Config.tempDir + "/" + workDir + "/" + Config.sim_java_output + "_" + assignID;
        } else {
            simFile = Config.tempDir + "/" + workDir + "/" + Config.sim_text_output + "_" + assignID;
        }*/
        simFile=Config.tempDir + "/" + workDir + "/"+ Config.getSimOutputFileName(Language.fromString(lang), assignID);
        String countStr = request.getParameter("count");

        displayAll(countStr, simFile, title,assignID);
        //display(count,simFile, title);

    }

    public void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response)
            throws javax.servlet.ServletException, java.io.IOException {
        doGet(request, response);

    }

    public void display(int count, String simFile, String title) {
        title = title.replace('|', ' ');
        pw.print("------------------------------------------------------------------------------");
        pw.println("------------------------------------------------------------------------------");
        pw.println("\n" + title + "\n");
        pw.print("------------------------------------------------------------------------------");
        pw.print("------------------------------------------------------------------------------\n\n");
        try {
            BufferedReader sim_br = new BufferedReader(new FileReader(simFile));
            String inLine = new String("");
            int lineCnt = 1;
            while ((inLine = sim_br.readLine()) != null && lineCnt != count) {
                lineCnt++;
            }
            pw.println(inLine);
            while ((inLine = sim_br.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(inLine, "|");
                if (st.countTokens() == 2) {
                    String p1 = st.nextToken();
                    String p2 = st.nextToken();
                    if (p1.startsWith("./") && p2.startsWith("./")) {
                        break;
                    }
                    pw.println(inLine);
                }
            }
            pw.flush();
            pw.close();
            sim_br.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception " + e);
        }
    }

    public void displayAll(String countStr, String simFile, String titleStr,String assignName) {
        titleStr = titleStr.replace('|', ' ');
        String[] titleArr = titleStr.split("::::::");
        String[] countArr = countStr.split(":");

        for (int i = 0; i < titleArr.length; i++) {

            String title = titleArr[i];
            if (countArr[i] == null || countArr[i].equals("")) {
                continue;
            }

            pw.print("------------------------------------------------------------------------------");
            pw.println("------------------------------------------------------------------------------");
            pw.println("\n" + title + "\n");
            pw.print("------------------------------------------------------------------------------");
            pw.print("------------------------------------------------------------------------------\n\n");
            try {
                int count = Integer.parseInt(countArr[i]);

                String inLine = new String("");
                int lineCnt = 1;
                BufferedReader sim_br = new BufferedReader(new FileReader(simFile));
                while ((inLine = sim_br.readLine()) != null && lineCnt != count) {
                    lineCnt++;
                }
                pw.println(inLine);
                while ((inLine = sim_br.readLine()) != null) {
                    StringTokenizer st = new StringTokenizer(inLine, "|");
                    if (st.countTokens() == 2) {
                        String p1 = st.nextToken();
                        String p2 = st.nextToken();
                        if (p1.startsWith(assignName) && p2.startsWith(assignName)) {
                            break;
                        }
                        pw.println(inLine);
                    }
                }
                sim_br.close();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Exception " + e);
            }
        }
        pw.flush();
        pw.close();

    }

}
