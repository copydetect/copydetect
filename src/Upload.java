/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;



public class Upload extends HttpServlet {

	private static final long serialVersionUID = 1L;
	String myTempDir = Config.tempDir;
	String workDir;
	int maxSizeK = Config.maxSizeK; 
	String studIDMapping = Config.studIDMapping;
	String assignIDMapping = Config.assignIDMapping;
	
	public synchronized void doPost(HttpServletRequest request, HttpServletResponse response) 
      throws IOException,ServletException{
		CreateTempDir();
		response.setContentType("text/html");
		PrintWriter outWriter = response.getWriter();
		ResponseWriter myResponse = new ResponseWriter(outWriter);
		myResponse.WriteHeader();
		if (ServletFileUpload.isMultipartContent(request)){
			DiskFileItemFactory dfi = new DiskFileItemFactory();
			dfi.setRepository(new File(myTempDir));
			dfi.setSizeThreshold(maxSizeK * 1024); //size in bytes
			ServletFileUpload servletFileUpload = new ServletFileUpload(dfi);
			try{
				String workDirName = "";
				String assignmentName = "";
                                List fileItemsList = servletFileUpload.parseRequest(request);
				Iterator it = fileItemsList.iterator();
				boolean flag = false;
				while (it.hasNext()){
				  FileItem fileItem = (FileItem)it.next();
				  if (fileItem.isFormField()){
					  //The file item contains a simple name-value pair of a form field 
				  }
				  else{
					  //The file item contains an uploaded file
					  workDirName = CreateRandExtDir();
					  workDir = myTempDir + "/" + workDirName;
					  String uploadName = fileItem.getName();
					  String ext = "";
					  if(uploadName.endsWith(".tar.gz")){
						  ext = ".tar.gz";
					  }else if(uploadName.endsWith(".tar")){
						  ext = ".tar";
					  }else if(uploadName.endsWith(".zip")){
						  ext = ".zip";
					  }else{
						  throw new Exception("Unknown backup file type.");
					  }
					  String filename = "backup" + ext;
					  File saveTo = new File(workDir + "/" + filename);
                                          assignmentName=uploadName.substring(0, uploadName.length()-ext.length());
                                          assignmentName=assignmentName.replace(" ","_");
					  fileItem.write(saveTo);
					  flag = true;
					  break;
				  }
				}
				if(!flag){
					myResponse.WriteError("No file found !");
				}
				myResponse.WriteStatus("File uploaded successfully.");

				//untar the backup file recursivley.
				
				//this untars the backup tar file into an assignment directory
				RecursiveUnTar rut = new RecursiveUnTar(workDir,assignmentName);
				rut.UnTar();
                                
				
				//check if the moodle backup format is present: moodle.xml and moddata
				/*File mdata = new File(workDir + "/" + "moddata");
				File mxml = new File(workDir + "/" + "moodle.xml");
				if(! mxml.exists()){
					throw new Exception("moodle.xml file missing.");
				}
				if( !mdata.exists()){
					throw new Exception("moddata directory missing in the backup file.");
				}*/
				//untar the files submitted by students
				//RecursiveUnTar ya_rut = new RecursiveUnTar(workDir + "/" + "moddata/assignment/");
                                RecursiveUnTar ya_rut = new RecursiveUnTar(workDir,null);
				int noOfTarFiles=1;
                                //while(noOfTarFiles!=0) {
                                    System.out.println("No of tar files:"+noOfTarFiles);
                                    noOfTarFiles=ya_rut.UnTar();
                                //}
				
                                System.out.println("Out of tar");
                                
				myResponse.WriteStatus("Recursive untar of the uploaded file done.");
				
				//convert all doc and pdf files to text format
				//DocPdfToText dptt = new DocPdfToText(workDir + "/" + "moddata/assignment/");
                                DocPdfToText dptt = new DocPdfToText(workDir );
				int count = dptt.ConvertToText();
				if(count > 0){
					myResponse.WriteStatus( count + " doc and pdf file(s) converted to text format.");
				}else{
					myResponse.WriteStatus( "No doc or pdf files found.");
				}
				
				//parse the moodle.xml file to create the mappings.
				//MoodleXML mxmlFile = new MoodleXML(workDir + "/" + "moodle.xml", workDir + "/" + studIDMapping, workDir + "/" + assignIDMapping);
				//mxmlFile.Parse();
				
				//myResponse.WriteStatus("Student-ID mapping and Assignment-ID mapping generated.");
				
                                
                                
                                myResponse.WriteProceedForm(workDirName,assignmentName);			
			} catch(SizeLimitExceededException e){
				myResponse.WriteError("file size limit exceeded. Max allowed size = " + maxSizeK + " KB.");
			} catch(FileUploadException e){
				e.printStackTrace();
				myResponse.WriteError("oops ... looks like a FileUploadException has occurred !");
				
			} catch(Exception e){
				e.printStackTrace();
				myResponse.WriteError("Following exception occurred -:");
				myResponse.WriteError(e.getMessage());
			} finally{
				myResponse.WriteFooter();
			}
		}
	}
  
	private void CreateTempDir(){
		File f = new File(myTempDir);
		if( ! f.exists()){
			f.mkdir();
		}
	}
	/**
	 * create a directory in the tempDir for processing the uploaded files. and return the name of that dir
	 */
	private String CreateRandExtDir(){
		long currTime = System.currentTimeMillis();
		String name = "copydetect_" + currTime;
		String dirPath = myTempDir + "/" + name;
		File f = new File(dirPath);
		if(f.exists()){
			f.delete();
		}
		f.mkdir();
		return name;
	}

}