

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;


public class DocPdfToText {

	//String startpt = "/home/naseer/ascwork/copydetect/moddata/";
	String startpt = "";
	String cmd = "ls -R";
	
	public DocPdfToText(String sp){
		startpt = sp;
		if(!startpt.endsWith("/")){
			startpt += "/";
		}
	}
	public int ConvertToText() throws Exception{
		int count = 0;
		File workDir = new File(startpt);
		Process p = Runtime.getRuntime().exec(cmd, null, workDir);
		//
		//if(i == 0)
                {
			BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String ip;
			String currdir = startpt;
			while((ip = br.readLine()) != null){
				if((ip.startsWith("./")) && (ip.endsWith(":"))){
					currdir = startpt + ip.substring(2, ip.length()-1) + "/";
				}else{
					if(ip.endsWith(".doc") ){
						String fname = ip.substring(0, ip.indexOf(".doc")) + ".txt";
						String docF = new String(currdir + ip);
						Process tt = Runtime.getRuntime().exec("antiword "+ docF + " > " + currdir + fname , null, new File(currdir));
						tt.waitFor();
						Process rr = Runtime.getRuntime().exec("rm "+ docF);
						rr.waitFor();
						count++;
					}else if(ip.endsWith(".pdf")) {
						String pdfF = new String(currdir + ip);
						Process zz = Runtime.getRuntime().exec("pdftotext "+ pdfF, null, new File(currdir));
						zz.waitFor();
						Process rr = Runtime.getRuntime().exec("rm "+ pdfF);
						rr.waitFor();
						count++;
					}
				}
			}
		}
                int i = p.waitFor();
		return count;
	}
	public static void main(String[] args){
		try{
			String startpt = "/home/naseer/ascwork/copydetect/moddata/";
			//start point can be given as cmd line arg
			if(args.length > 0){
				if(args[0].equals("-sp")){
					startpt = args[1];
				}
			}
			DocPdfToText dptt = new DocPdfToText(startpt);
			dptt.ConvertToText();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("usage: java RecursiveUnTar -sp <startPointOfDir>");
		}
	}
}
