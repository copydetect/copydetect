
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <ul>
 * <li>Takes as input the internal directory where the backup directory was
 * uploaded.
 * <li>displays the available assignments for detecting copies.
 * <li>It is called after Upload.java uploads the files.
 *
 * @author rosecatherinek
 */
public class SimProcess extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public synchronized void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String state = request.getParameter("state");
        System.out.println("parameter state = " + state);
        state = state.toLowerCase();
        if (state.equals("start")) {
            Start(request, response);
        } else if (state.equals("runsim")) {
            RunSim(request, response);
        }

    }

    public synchronized void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doGet(request, response);

    }

    private void Start(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html");
        PrintWriter outWriter = response.getWriter();
        ResponseWriter myResponse = new ResponseWriter(outWriter);
        myResponse.WriteHeader();
        myResponse.WriteSimInstructions();

        try {
            //display the availabe assignments by reading from the assignment-ID mapping
            String workDir = request.getParameter("dir");
            //String assignIDMap = Config.tempDir + "/" + workDir + "/" + Config.assignIDMapping;
            //myResponse.WriteAssignmentTable(assignIDMap, workDir);
            String assignmentName = request.getParameter("assignmentName");
            myResponse.WriteTemplate(assignmentName, workDir);
        } catch (Exception e) {
            e.printStackTrace();
            myResponse.WriteError("Following exception occurred:");
            myResponse.WriteError(e.getMessage());
        } finally {
            myResponse.WriteFooter();
        }
    }

    private void RunSim(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html");
        PrintWriter outWriter = response.getWriter();
        ResponseWriter myResponse = new ResponseWriter(outWriter);
        myResponse.WriteHeader();
        try {
            //String assignID = request.getParameter("assignid");
             String assignName = request.getParameter("assignmentName");
            String workDir = request.getParameter("dir");
            String workDirName = workDir;
            workDir = Config.tempDir + "/" + workDir+"/";
            //check if the files for this assignment are present in the backup 
            //String assignDirName = workDir + "/moddata/assignment/" + assignID;
            String assignDirName = workDir;
            if (!(new File(assignDirName)).exists()) {
                throw new Exception("Files for this assignment not found in the backup.");
            }

            //String assignName = myResponse.WriteAssignmentName(workDir + "/" + Config.assignIDMapping, assignID);
           
            //Run sim on the files in this assignment. output goes to sim_*_output in the workDir.

            //Choose the language
            String lang = request.getParameter("lang");

            File templateFile = new File(Config.getTemplateFileName(workDir + "/", assignName, Language.fromString(lang)));
            String templateCode = request.getParameter("templateCode");
            BufferedWriter bw = new BufferedWriter(new FileWriter(templateFile));
            bw.write(templateCode);
            bw.close();

            if (lang.equals("c")) {
                Sim_c sc = new Sim_c(workDir , workDir + "/" + Config.getSimOutputFileName(Language.fromString(lang),assignName));
                //int count = sc.ExecSim("assignment/" + assignName);
                int count=sc.ExecSim(assignName);
                if (count <= 0) {
                    myResponse.WriteError("No C,C++ files found !");
                } else {
                    //parse sim output and display the output
                    SortListStudPairs slsp = new SortListStudPairs(workDir + "/" + Config.studIDMapping, workDir + "/" + Config.getSimOutputFileName(Language.fromString(lang),assignName), outWriter, workDirName, lang,  assignName);
                    slsp.ParseSimOutput(assignName);
                    myResponse.WriteStatus("<br>Number of C,C++ file(s) processed : " + count);
                }
            } else if (lang.equals("java")) {
               
                Sim_java sj = new Sim_java(workDir, Config.getSimOutputFileName(Language.fromString(lang),assignName));
                int count = sj.ExecSim("" + assignName);
                if (count <= 0) {
                    myResponse.WriteError("No Java files found !");
                } else {
                    //parse sim output and display the output
                    System.out.println("Java file processing------------");
                    SortListStudPairs slsp = new SortListStudPairs(workDir + "/" + Config.studIDMapping, workDir + "/" + Config.getSimOutputFileName(Language.fromString(lang),assignName), outWriter, workDirName, lang,  assignName);
                    slsp.ParseSimOutput(assignName);
                    myResponse.WriteStatus("<br>Number of Java file(s) processed : " + count);
                }
            } else if (lang.equals("lisp")) {
                Sim_lisp sc = new Sim_lisp(workDir , workDir + "/" + Config.getSimOutputFileName(Language.fromString(lang),assignName));
                int count = sc.ExecSim( assignName);
                if (count <= 0) {
                    myResponse.WriteError("No Lisp files found !");
                } else {
                    //parse sim output and display the output
                    SortListStudPairs slsp = new SortListStudPairs(workDir + "/" + Config.studIDMapping, workDir + "/" + Config.getSimOutputFileName(Language.fromString(lang),assignName), outWriter, workDirName, lang,  assignName);
                    slsp.ParseSimOutput(assignName);
                    myResponse.WriteStatus("<br>Number of Lisp file(s) processed : " + count);
                }
            }    
            else {
                //default is text
                Sim_text st = new Sim_text(workDir , workDir + "/" + Config.getSimOutputFileName(Language.fromString(lang),assignName));
                int count = st.ExecSim("" + assignName);
                if (count <= 0) {
                    myResponse.WriteError("No Text files found !");
                } else {
                    //parse sim output and display the output
                    SortListStudPairs slsp = new SortListStudPairs(workDir + "/" + Config.studIDMapping, workDir + "/" + Config.getSimOutputFileName(Language.fromString(lang),assignName), outWriter, workDirName, lang,  assignName);
                    slsp.ParseSimOutput(assignName);
                    myResponse.WriteStatus("<br>Number of Text file(s) processed : " + count);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            myResponse.WriteError("Following exception occurred :");
            myResponse.WriteError(e.getMessage());
        } finally {
            myResponse.WriteFooter();
        }
    }

}
