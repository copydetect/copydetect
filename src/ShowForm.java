

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ShowForm extends HttpServlet{
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException,ServletException {
		doGet(req,res);
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
    	response.setContentType("text/html");
		PrintWriter outWriter = response.getWriter();
		ResponseWriter myResponse = new ResponseWriter(outWriter);
		myResponse.WriteHeader();
		myResponse.WriteUploadInstructionsTableFormat();
		myResponse.WriteUploadForm();
		myResponse.WriteBackupInstructionsTableFormat();
		myResponse.WriteFooter();
    }

}
