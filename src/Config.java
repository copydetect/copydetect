
public class Config {
/** Change the following paths **/
  public static String tempDir = "/tmp/CopyDetectTemp";

 private static String simRoot = "/home/banks/copydetect/SIMCore/install/bin.x86_64/"; 
 public static String clusteringRunnableJar = "java -jar /home/banks/copydetect/Clustering.jar";
  
  /** Not to be changed **/
  public static int maxSizeK = 5000; //5M 
  public static String studIDMapping = "studIDMapping.txt";
  public static String assignIDMapping = "assignIDMapping.txt";
  
  
  public static String sim_c = Config.simRoot + "sim_c";
  public static String sim_java = Config.simRoot + "sim_java";
  public static String sim_lisp = Config.simRoot + "sim_lisp";
  public static String sim_text = Config.simRoot + "sim_text";
  
  //public static String sim_c_output = "sim_c_output";
  //public static String sim_java_output = "sim_java_output";
 // public static String sim_text_output = "sim_text_output";
  private static final String SIM_C_MOD_OUTPUT = "sim_c_output_new";
  private static final String SIM_JAVA_MOD_OUTPUT = "sim_java_output";
  private static final String SIM_LISP_MOD_OUTPUT = "sim_lisp_output";
  private static final String SIM_TEXT_MOD_OUTPUT = "sim_text_output_new";
  
  
  
  
  private static String getSimExecutable(Language language) {
    switch (language) {
      case CPP_C:
        return sim_c;
      case JAVA:
        return sim_java;
      case LISP:
        return sim_lisp;
      default:
        return sim_text;
    }
  }
  
  public static final String TEMPLATE_FILE = "templateFile";
  
  public static String[] getSimBaseCommandArgs(Language language, String workingDir,
          String assignId) {
    String[] retVal = {getSimExecutable(language), "-s","-w","200", "-m", 
      getTemplateFileName(workingDir, assignId, language),"-o",
      workingDir + "" + getSimOutputFileName(language, assignId)};
    return retVal;
  }
  
  /**
   * Returns the name of the output file where the output of execution will be
   * saved.
   * 
   * @param language
   * @return Name of the output file.
   */
  public static String getSimOutputFileName(Language language, String assignId) {
    switch (language) {
      case CPP_C:
        return SIM_C_MOD_OUTPUT + "_" + assignId;
      case JAVA:
        return SIM_JAVA_MOD_OUTPUT + "_" + assignId;
      case LISP:
        return SIM_LISP_MOD_OUTPUT + "_" + assignId;
      default:
        return SIM_TEXT_MOD_OUTPUT + "_" + assignId;
    }
  }

  public static String getTemplateFileName(String workingDir, String assignId, Language lang) {
    return workingDir +  TEMPLATE_FILE + "_" + assignId + "_"  + lang.getValue();
  }
  
  
  
}
