import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class Sim_lisp {
	String startPt;
	String output;
	public Sim_lisp(String wd, String op){
		startPt = wd;
		output = op;
		if(!startPt.endsWith("/")){
			startPt += "/";
		}
	}
	public int ExecSim(String prefix) throws Exception {
        ArrayList<String> argList = new ArrayList<String>();
        File workDir_ls = new File(startPt+prefix);
        Process p = Runtime.getRuntime().exec("ls -R", null, workDir_ls);

        BufferedReader lsbr = new BufferedReader(new InputStreamReader(p.getInputStream()));

        // BufferedReader lsbr = new BufferedReader(new FileReader(new File(startPt+prefix+"lsOut")));
        BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String ip;
        String currdir = "";
        while ((ip = br.readLine()) != null) {
            if ((ip.startsWith("./")) && (ip.endsWith(":"))) {
                currdir = ip.substring(2, ip.length() - 1) + "/";
            } else {
                String fullName = startPt + prefix + "/" + currdir + ip;
                if (FileType.isLisp(fullName)) {
                    String lispF = currdir + ip;
                    argList.add(lispF);
                }
            }
        }

        int w = p.waitFor();
        System.out.println("Listed all files to process........");
        if (w == 0) {
            
            System.out.println("Number of lisp files = " + argList.size());
            
            String[] base_sim_cmd = Config.getSimBaseCommandArgs(Language.fromString("lisp"), startPt, prefix);

            String[] sim_cmd = new String[base_sim_cmd.length + argList.size()];
            System.arraycopy(base_sim_cmd, 0, sim_cmd, 0, base_sim_cmd.length);

            for (int i = 0; i < argList.size(); i++) {
                sim_cmd[i + base_sim_cmd.length] = prefix + "/" + argList.get(i);
            }
            File workDir = new File(startPt);
            Process tt = Runtime.getRuntime().exec(sim_cmd, null, workDir);
            tt.waitFor();
            System.out.println("Output of running sim--" + tt.exitValue());
        }
        return argList.size();
    }
}
