
public enum Language {
  JAVA("java"), CPP_C("c"), TEXT("text"), LISP("lisp");
  private String value;

  Language(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  public static Language fromString(String lang) {
    if (lang != null) {
      for (Language b : Language.values()) {
        if (lang.equalsIgnoreCase(b.value)) {
          return b;
        }
      }
    }
    return null;
  }
 
  @Override
  public String toString() {
    return value;
  }
}
