

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;


/**
 * SortListStudPairs is similar to YAListStudPairs, except for the following:
 * <ul>
 * <li>Lists the student pairs (rollno, name) and the <b>sum</b> of the runlengths of detected copy (with line number 
 * in the sim output file of the corresponding snippets, as comma separated values).
 * <li>The pairs are listed in the <b>decreasing order of sum of runlengths</b>. 
 * </ul>
 * @author rosecatherinek
 */
public class SortListStudPairs {

	String mappingFile = new String("");
	String simFile = new String("/home/naseer/ascwork/copydetect/sim_output/270_text.txt");
	PrintWriter outWriter;
	HashMap<String, StudRecord> hm;
	String workDirName;
	String lang;
	
	String assignName;

	public SortListStudPairs(String mf, String sf, PrintWriter ow, String wd, String l,  String assName) throws Exception{
		mappingFile = mf;
		simFile = sf;
		outWriter = ow;
		workDirName = wd;
		lang = l;
		assignName = assName;
		//InitMapping();
	}
	private void InitMapping() throws Exception{
		System.out.println("Initializing Student-ID hash map");
		//assumes that in the mapping file, the data is entered as: <moodleid> | <studentName> | <rollNo>
		//to read contents from the mapping file
		BufferedReader br = new BufferedReader(new FileReader(mappingFile));
		hm = new HashMap<String, StudRecord>();
		String line = new String("");
		while( (line = br.readLine()) != null){
			StringTokenizer st = new StringTokenizer(line, "|");
			int moodId = Integer.parseInt(st.nextToken().trim());
			hm.put(moodId+"", new StudRecord(st.nextToken(), st.nextToken()));
		}
		br.close();
	}
	public void ParseSimOutput(String assignmentName) throws Exception{

		//IMP: the structure of the entries are assumed to be:
		// assignment/<assignNo>/<studMoodleId>/<file details> |
		// assignment/<assignNo>/<studMoodleId>/<file details> [<run length>]
                hm = new HashMap<String, StudRecord>();
		HashMap<String, SumFileData> copyPairHM = new HashMap<String, SumFileData>();
		BufferedReader sim_br = new BufferedReader(new FileReader(simFile));
		String inLine = new String("");
		int lineCnt = 0;
                boolean showRoll=false;
		while((inLine = sim_br.readLine()) != null){
			lineCnt ++;
			StringTokenizer st = new StringTokenizer(inLine, "|");
			if(st.countTokens() == 2){
				String p1 = st.nextToken();
				String p2 = st.nextToken();
				if(p1.startsWith(assignmentName) && p2.startsWith(assignmentName))
                                {
					//sample p1: assignment/270/2625/06005011_ass1.txt: line 3-8 
					//sample p2: assignment/270/2639/06005001_01.sql: line 13-18[42]
					
					//get the file names
					int colIndex1 = p1.indexOf(":");
					if(colIndex1 < 0){
						//some error?
						continue;
					}
					String file1 = p1.substring(0, colIndex1);
					int colIndex2 = p2.indexOf(":");
					if(colIndex2 < 0){
						//some error?
						continue;
					}
					String file2 = p2.substring(0, colIndex2);
					
					//check if this pair is valid
					StringTokenizer p1st = new StringTokenizer(p1, "/");
					//p1st.nextToken(); //removes name
					String ass1 = p1st.nextToken(); //removes assign
					//int stud1 = Integer.parseInt(p1st.nextToken().trim());
                                        String stud1Details=p1st.nextToken().trim();
                                        int stud1index = stud1Details.lastIndexOf("_assignsubmission");
                                        if(stud1index==-1) stud1index=stud1Details.length();
                                        
                                       String stud1= stud1Details.substring(0,stud1index).split("\\.")[0];
                                       // String stud1= p1st.nextToken();
                                        
                                       // String[] p2Arr=p2.split("line",-2);
                                        
					StringTokenizer p2st = new StringTokenizer(p2, "/");
                                     
                                        
                                        
                                        
					//p2st.nextToken(); //removes assignment
					String ass2 = p2st.nextToken(); //removes <assignNo>
					//int stud2 = Integer.parseInt(p2st.nextToken().trim());
                                        String stud2Details=p2st.nextToken().trim();
                                        int stud2index = stud2Details.lastIndexOf("_assignsubmission");
                                         if(stud2index==-1) stud2index=stud2Details.length();
                                        
                                        String stud2= stud2Details.substring(0,stud2index).split("\\.")[0];
                                        //String stud2=p2st.nextToken();
                                        
                                       
                                       
                                        
					if(stud1.equalsIgnoreCase(stud2)){
						continue;
					}
					if(!ass1.equals(ass2)){
						continue;
					}
					//to get run length
                                        
                                        if(hm.get(stud1)==null) {
                                          
                                         if(stud1.matches("[0-9]+") && showRoll){
                                            hm.put(stud1, new StudRecord(stud1Details.substring(0,stud1index),stud1));
                                         }
                                         
                                         else{
                                            hm.put(stud1, new StudRecord(stud1,stud1Details));
                                            showRoll=false;
                                         }
                                        }
                                        if(hm.get(stud2)==null) {
                                           if(stud2.matches("[0-9]+") && showRoll){
                                             hm.put(stud2, new StudRecord(stud2Details.substring(0,stud2index),stud2));
                                           } else {
                                            hm.put(stud2, new StudRecord(stud2,stud2Details));
                                            showRoll=false;
                                           }
                                        }
                                        
                                      //  System.out.println("Student1 roll:"+ stud1+"   Student2 roll"+stud2);
                                        
					String remp2 = new String("");
					while(p2st.hasMoreTokens()){
						//to get the last token
						remp2 = p2st.nextToken();
					}
                                        
                                       remp2=p2;
                                       
					String rLenStr = remp2.substring(remp2.indexOf("[") + 1, remp2.indexOf("]"));
					int rlen = Integer.parseInt(rLenStr);
					String larger,smaller;
                                        
                                        if(stud1.compareTo(stud2)>0){
                                            larger=stud1;
                                            smaller=stud2;
                                        }else{
                                            larger=stud2;
                                            smaller=stud1;
                                        }
                                        
                                        String HMkey = smaller + ":" + larger;
					if(!copyPairHM.containsKey(HMkey)){
						copyPairHM.put(HMkey, new SumFileData());
					}
					SumFileData entry = copyPairHM.get(HMkey);
					entry.add(rlen, new SnippetData(file1, file2, lineCnt));
				}
			}
		}

		//get the data from the hashmap for sorting
		Set HMKeySet = copyPairHM.keySet();
		String[] keySetArray = new String[HMKeySet.size()];
		int[] totRunLenArray = new int[HMKeySet.size()];
		Iterator copyIT = HMKeySet.iterator();
		for(int i = 0 ; i < HMKeySet.size(); i++){
			String HMkey = (String) copyIT.next();
			int totrl = copyPairHM.get(HMkey).GetTotalRunLength();
			keySetArray[i] = HMkey;
			totRunLenArray[i] = totrl;
		}
		sim_br.close();
		//sort
		MyQuickSortIntStringArrays.quicksortIntString(totRunLenArray, keySetArray, keySetArray.length);

		//write the output in html format:
		outWriter.println("<table border=2>");
                if(showRoll){
                    outWriter.println("<tr> <td><b>stud1-rollno</b> <td><b>stud1-name</b> <td><b>stud2-rollno</b> <td><b>stud2-name</b> <td><b>total run len</b> <td><b>snippets</b> </tr>");
                } else {
                    outWriter.println("<tr>  <td><b>stud1-name</b> <td><b>stud2-name</b> <td><b>total run len</b> <td><b>snippets</b> </tr>");
                }
		for(int i = 0; i < keySetArray.length; i++){
			String HMkey = keySetArray[i];
			StringTokenizer copyST = new StringTokenizer(HMkey, ":");
			StudRecord stud1 = hm.get(copyST.nextToken());
			StudRecord stud2 = hm.get(copyST.nextToken());
			if(stud1==null || stud2==null)
                            continue;
                        
                        //System.out.println("Student1"+stud1+"  Student2"+stud2+ "hm size"+hm.size());
                        if(showRoll){
                            outWriter.println("<tr> <td>" + stud1.rollNo + " <td> " + stud1.name + " <td> " + stud2.rollNo + " <td> " + stud2.name );
                        } else {
                            outWriter.println("<tr> <td> " + stud1.name + " <td> " + stud2.name );
                        }
			SumFileData fd = copyPairHM.get(HMkey);
			int totRunLen = fd.runLen;
			outWriter.println( " <td> " + totRunLen + " <td> ");
			ArrayList<SnippetData> snippetList = fd.snippetList;
			String allTitle="",allCount="";
                        for(int k = 0; k<snippetList.size(); k++){
				String snippetTitle = "Assignment: " + assignName + "        Student1: " + stud1.name + "(" + stud1.rollNo + ")        Student2: " + stud2.name + "(" + stud2.rollNo + ")"; 
				String stud1Title = "Name: " + stud1.name + " Roll no: " + stud1.rollNo + "        Assignment: " + assignName;
				String stud2Title = "Name: " + stud2.name + " Roll no: " + stud2.rollNo + "        Assignment: " + assignName;
				snippetTitle = snippetTitle.replace(' ', '|');
				stud1Title = stud1Title.replace(' ', '|');
				stud2Title = stud2Title.replace(' ', '|');
				String file1ShortName = snippetList.get(k).file1;
				int nameStart1 = file1ShortName.lastIndexOf("/");
				file1ShortName = file1ShortName.substring(nameStart1 + 1);
				String file2ShortName = snippetList.get(k).file2;
				int nameStart2 = file2ShortName.lastIndexOf("/");
				file2ShortName = file2ShortName.substring(nameStart2 + 1);
				
				if(k != 0){
					outWriter.println("<br>");
				}
				outWriter.print( "<a href=DisplaySnippet?count="+ snippetList.get(k).lineNum + "&dir=" + workDirName + "&lang=" + lang + "&assignid=" + assignName + "&title=" + snippetTitle + " target=\"_blank\">id"  + snippetList.get(k).lineNum + "</a>");
				outWriter.print( " (<a href=DisplayFile?name="+ snippetList.get(k).file1.replace(" ", "%20") + "&dir=" + workDirName + "&title=" + stud1Title + " target=\"_blank\">file1: " + file1ShortName + "</a>");
				outWriter.println( " , &nbsp;<a href=DisplayFile?name="+ snippetList.get(k).file2.replace(" ", "%20") + "&dir=" + workDirName + "&title=" + stud2Title + " target=\"_blank\">file2: " + file2ShortName + "</a>) ");
                                allCount+=snippetList.get(k).lineNum+":";
                                allTitle+=snippetTitle+"::::::";
			}
                        
                        //outWriter.print( "<br/><a href=DisplaySnippet?count="+ allCount + "&dir=" + workDirName + "&lang=" + lang + "&assignid=" + assignName + "&title=" + allTitle + " target=\"_blank\">Diff of all similar files"  + "" + "</a>");
                        
		}
		outWriter.println("</table>");
		outWriter.flush();
	}
	
}
